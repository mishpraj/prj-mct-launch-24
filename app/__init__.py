"""from flask import Flask, request, jsonify, send_from_directory,send_file
from typing import List, Dict
import os 
import math 
import random 
import string 
import json 
import requests
import datetime
import time 
import shutil 
import pickle
import stat

app = Flask(__name__)
@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return 'No file part in the request', 400
    files= request.files.getlist('file')
    statement:str="OK"
    for f in files:
        file_name:str = f.filename.split("/")[-1]
        if os.path.isfile(current_dir+"/"+Storage_Compartment+"/"+file_name):
            if statement == "OK": 
                statement = f"File already exists {file_name}"
            else: 
                statement+=f"File already exists {file_name}"
            with open(Logs,append_mode) as logfile:
                logfile.write(f"[Ops_Savefile:002/F/A] File already exists\t{datetime.datetime.now()}\t{file_name}\n")
        with open(current_dir+"/"+Storage_Compartment+"/"+file_name,"wb") as file_to_write:
            file_to_write.write(f.read())
        with open(Logs,append_mode) as logfile:
            logfile.write(f"[Ops_Savefile:003/F/A] Uploaded File\t{datetime.datetime.now()}\t{file_name}\n")
    data:Dict[str,str,str] =  request.form.to_dict()
    return statement,200
@app.route("/view",methods=["GET"])
def view():
    return jsonify(os.listdir(current_dir+"/"+Storage_Compartment)),200
@app.route("/view_file",methods=["GET"])
def view_file():
    filename:str = request.args.get("filename")
    if filename is None:
        return "No filename specified",400
    if not os.path.isfile(current_dir+"/"+Storage_Compartment+"/"+filename):
        return "File does not exist",400
    with open(Logs,append_mode) as f:
        f.write(f"[Ops_viewfile:004/F/A] Viewed File\t{datetime.datetime.now()}\t{filename}\n")
    return send_from_directory(current_dir+"/"+Storage_Compartment,filename,as_attachment=False)
@app.route("/download",methods=["GET"])
def download():
    data:Dict[str,str,str] =  request.form.to_dict()
    filename:str = data["filename"]
    return send_from_directory(current_dir+"/"+Storage_Compartment,filename,as_attachment=True)
@app.route("/health_check",methods=["GET","POST","PUT","DELETE","PATCH","OPTIONS","HEAD","TRACE","CONNECT"])
def health_check():
    return "OK",200
@app.route('/manifest')
def manifest():
    data = {
        'key1': 'value1',
        'key2': 'value2',
        # Add more key-value pairs as needed
    }
    return jsonify(data)
"""
from flask import Flask, request, jsonify, send_from_directory, send_file, make_response
from typing import List, Dict
import os 
import math 
import random 
import string 
import json 
from PIL import Image, ImageDraw, ImageFont
import requests
import datetime
import time 
import io
import shutil 
import pickle
import stat
import pymysql
import redis
import base64
def set_read_only(dir_path:str):
    os.chmod(dir_path, stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH)
def set_write_only(dir_path:str):
    os.chmod(dir_path, stat.S_IWRITE | stat.S_IWGRP | stat.S_IWOTH)
def set_read_write(dir_path:str):
    os.chmod(dir_path, stat.S_IWRITE | stat.S_IWGRP | stat.S_IWOTH | stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH)

def backup_files(source_dir:str, backup_dir:str):
    for filename in os.listdir(source_dir):
        shutil.copy2(os.path.join(source_dir, filename), backup_dir)
        write_log(f"Backed up file\t{filename}","Ops_backup:001/F/A",Logsfile)
    return True
def schedule_backup(source_dir:str, backup_dir:str, interval:int):
    while True:
        backup_files(source_dir, backup_dir)
        time.sleep(interval)
def write_log(content:str,method:str,log_file:str="Logs"): 
    with open(log_file,append_mode) as f:
        f.write(f"[{method}] {content}\t{datetime.datetime.now()}\n")
    return True
def get_random_string(length:int):
    letters:str = string.ascii_lowercase
    result_str:str = ''.join(random.choice(letters) for i in range(length))
    return result_str
def transform_image(file, ext, width=None, height=None,angle=None):
    # Assume the image is sent in the request as a file in jpeg format
    image = Image.open(os.path.join(current_dir+"/"+Storage_Compartment,file+".jpeg"))
    
    # If width and height are not provided, keep the original dimensions
    width = width or image.size[0]
    height = height or image.size[1]

    # Resize the image
    resized_image = image.resize((width, height))
    #rotate the image
    if angle is not None:
        # Convert the image to RGBA if not already to ensure it has an alpha channel
        if resized_image.mode != 'RGBA':
            resized_image = resized_image.convert('RGBA')

        # Rotate the image and fill extra space with a transparent color
        resized_image = resized_image.rotate(angle, expand=True, fillcolor=(0, 0, 0, 0))

    # Save the resized image to a BytesIO object
    byte_io = io.BytesIO()
    resized_image.save(byte_io, format=ext.upper())
    byte_io.seek(0)

    # Send the resized image as a response
    return send_file(byte_io, mimetype='image/{}'.format(ext))
#Redis Conn details
Conn_URL:str = "rediss://clsds3cache01ac2701024:AVNS_6dFoJx68jgLAFisnOxq@redis-129effed-pretreer-70a3.aivencloud.com:20814"
client_redis = redis.Redis.from_url(Conn_URL)

#MySQL Conn details
Conn_timeout:int = 10 # time out in seconds
mySQL_conn =  pymysql.connect(
  charset="utf8mb4",
  connect_timeout=Conn_timeout,
  cursorclass=pymysql.cursors.DictCursor,
  db="defaultdb",
  host="mysql-1bbc8d02-pretreer-70a3.aivencloud.com",
  password="AVNS_1H8It55P8mG74yZj17W",
  read_timeout=Conn_timeout,
  port=20813,
  user="avnadmin",
  write_timeout=Conn_timeout,
)
cache_image:bool=False
current_dir:str = os.getcwd()
current_dir = current_dir.replace("\\","/")
Storage_Compartment:str = "storage-br11"
backupStorage_Compartment:str = "backup-storage-stb-294"
Logsfile:str = "Logs"
append_mode:str = "a"
if not os.path.isdir(current_dir+"/"+Storage_Compartment):
    os.mkdir(current_dir+"/"+Storage_Compartment)
    write_log(f"Created Storage Compartment\t{Storage_Compartment}","Ops_makefile:001/F/A",Logsfile)
elif not os.path.isdir(current_dir+"/"+backupStorage_Compartment):
    os.mkdir(current_dir+"/"+backupStorage_Compartment)
    write_log(f"Created Storage Compartment\t{backupStorage_Compartment}","Ops_makefile:001/F/B",Logsfile)
else:
    pass 

app = Flask(__name__)
@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return 'No file part in the request', 400
    files= request.files.getlist('file')
    statement:str="OK"
    for f in files:
        file_name:str = f.filename.split("/")[-1]
        if os.path.isfile(current_dir+"/"+Storage_Compartment+"/"+file_name):
            if statement == "OK": 
                statement = f"File already exists {file_name}"
            else: 
                statement+=f"File already exists {file_name}"
            write_log(f"File already exists\t{file_name}","Ops_Savefile:002/F/A",Logsfile)
        with open(current_dir+"/"+Storage_Compartment+"/"+file_name,"wb") as file_to_write:
            file_to_write.write(f.read())
        write_log(f"Uploaded File\t{file_name}","Ops_Savefile:003/F/A",Logsfile)
    # data:Dict[str,str,str] =  request.form.to_dict()
    return statement,200
@app.route('/upload/<method>/<path:url>', methods=['POST','GET'])
def upload_from_url(method, url):
    # Handle upload from URL here
    # The method of saving can be determined by the 'method' variable
    if method == 'url':
        # Save using URL method
        Data_URL:str = url
        print(Data_URL)
        File_name:str = Data_URL.split("/")[-1]
        if os.path.isfile(current_dir+"/"+Storage_Compartment+"/"+File_name):
            return "File already exists",400
        r = requests.get(Data_URL, allow_redirects=True)
        if r.status_code != 200:
            return "URL is invalid",400
        #if the size of the file is 0 or is not a valid media then return error
        if len(r.content) == 0:
            return "URL is invalid",400
        with open(current_dir+"/"+Storage_Compartment+"/"+File_name, 'wb') as f:
            f.write(r.content)
        write_log(f"Uploaded File\t{File_name}","Ops_Savefile/URL:001/S/A",Logsfile)
        return "OK",200
    elif method == 'b64':
        # Save using base64 method
        # Data_Encode:str = data
        #save the base64    encoded file 
        pass
    else:
        # Handle other methods
        pass
@app.route("/view",methods=["GET"])
def view():
    return jsonify(os.listdir(current_dir+"/"+Storage_Compartment)),200
@app.route("/view_file",methods=["GET"])
def view_file():
    File_name:str = request.args.get("file")
    Fileid:str = request.args.get("FUUid")
    Api_key:str = request.args.get("key")
    Auth_token:str = request.args.get("auth_token")
    Version:str = request.args.get("version")
    format:str = request.args.get("format") or None
    if File_name is None:
        write_log(f"No filename specified","Ops_viewfile:004/F/A",Logsfile)
        return "No filename specified",400
    if not os.path.isfile(current_dir+"/"+Storage_Compartment+"/"+File_name):
        return "File does not exist", 404
    write_log(f"Viewed File\t{File_name}","Ops_viewfile:004/F/A",Logsfile)
    mime_type:str = "" 
    if format is None:
        mime_type = File_name.split(".")[-1]
    else:
        mime_type = format
    if mime_type == "jpg" or mime_type == "jpeg" or mime_type == "png" or mime_type == "gif":
        mime_type = "image/"+mime_type
    elif mime_type == "mp4" or mime_type == "avi" or mime_type == "mkv" or mime_type == "webm":
        mime_type = "video/"+mime_type
    elif mime_type == "mp3" or mime_type == "wav" or mime_type == "ogg" or mime_type == "flac":
        mime_type = "audio/"+mime_type
    else:
        mime_type = "application/"+mime_type

    response = make_response(send_file(current_dir+"/"+Storage_Compartment+"/"+File_name, mimetype=mime_type))
    response.headers.set('Cache-Control', 'public, max-age=6000, s-maxage=6000')
    # return send_from_directory(current_dir+"/"+Storage_Compartment,File_name,as_attachment=False)
    return response
@app.route("/download",methods=["GET"])
def download():
    data:Dict[str,str,str] =  request.form.to_dict()
    filename:str = data["filename"]
    return send_from_directory(current_dir+"/"+Storage_Compartment,filename,as_attachment=True)
@app.route("/healthcheck",methods=["GET","POST","PUT","DELETE","PATCH","OPTIONS","HEAD","TRACE","CONNECT"])
def health_check():
    return "OK",200
@app.route("/delete", methods=["POST","DELETE"])
def delete_file(file_name:str=None,called_internal:bool=False):
    if not called_internal:
        data:Dict[str,str,str] =  request.form.to_dict()
        file_name:str = data["filename"]
        compartment = Storage_Compartment
        if file_name is None:
            return "No filename specified",400
        if not os.path.isfile(current_dir+"/"+compartment+"/"+file_name):
            return "File does not exist",400
        os.remove(current_dir+"/"+compartment+"/"+file_name)
        write_log(f"Deleted File\t{file_name}","Ops_deletefile:006/F/A",Logsfile)
        return "OK",200
    else:
        if file_name is None:
            write_log(f"No filename specified","Ops_delete:004/F/A",Logsfile)
            return "No filename specified",400
        if not os.path.isfile(current_dir+"/"+compartment+"/"+file_name):
            write_log(f"File does not exist\t{file_name}","Ops_delete:005/F/B",Logsfile)
            return "File does not exist",400
        os.remove(current_dir+"/"+compartment+"/"+file_name)
        write_log(f"Deleted File\t{file_name}","Ops_deletefile:006/F/A",Logsfile)
        return "OK",200
    pass
@app.route("/transform",methods=["POST"])
def transform_media():
    #instructions 
    Crop__:str = request.arg.get("crop")
    Angle__:str = request.arg.get("angle")
    Extension__:str = request.arg.get("extension")
    Width__:str = request.arg.get("width")
    Height__:str = request.arg.get("height")
    Quality__:str = request.arg.get("quality")
    Mode__:str = request.arg.get("mode")
    Audio_Mode__:str = request.arg.get("audio_codec")
    Color_Value:str=request.arg.get("color")
    Text_Value__:str = request.arg.get("text")
    Font_Value__:str = request.arg.get("font")
    Font_Size__:str = request.arg.get("font_size")
    Scale__:str = request.arg.get("scale")
    #end of instructions

    pass

@app.route('/complete_healthcheck')
def manifest():
    data = {
        'key1': 'value1',
        'key2': 'value2',
        # Add more key-value pairs as needed
    }
    return jsonify(data)
@app.route('/transform/w=<int:width>,h=<int:height>,a=<angle>/<file>.<extenstion>', methods=['GET','POST','PUT','DELETE','PATCH','OPTIONS','HEAD','TRACE','CONNECT'])
def transform_I_W_S_A(width, height,angle, file, extenstion):#I_W_S = Image_With_Size_and_Angle
    return transform_image(file, extenstion, width, height,int(angle))
@app.route('/transform/<path:file>.<extenstion>', methods=['GET','POST','PUT','DELETE','PATCH','OPTIONS','HEAD','TRACE','CONNECT'])
def transform_I__ORI(file, extenstion):#I__ORI = Image_Original
    if extenstion == "jpg":
        extenstion = "jpeg"
    return transform_image(file, extenstion)
@app.route('/transform/w=<int:width>,h=<int:height>/<file>.<extenstion>', methods=['GET','POST','PUT','DELETE','PATCH','OPTIONS','HEAD','TRACE','CONNECT'])
def transform_I_W_H(width, height, file, extenstion): #I_W_H = Image_With_Width_and_Height
    return transform_image(file, extenstion, width, height)

# def resize_image(width, height,file,extenstion):
#     with Image.open(os.path.join(current_dir+"/"+Storage_Compartment,file+".jpeg")) as im:
#         if width == -1 or height == -1:
#             width, height = im.size
#         im = im.resize((width, height))

#     byte_io= io.BytesIO()
#     if extenstion == "jpg":
#         extenstion = "jpeg"
#     im.save(byte_io, extenstion)
#     byte_io.seek(0)
#     return send_file(byte_io, mimetype='image/'+extenstion)

    