from app import app
from flask import Flask, request, jsonify, send_from_directory,send_file
from typing import List, Dict
import os 
import math 
import random 
import string 
import json 
import requests
import datetime
import time 
import shutil 
import pickle
import stat
def set_read_only(dir_path:str):
    os.chmod(dir_path, stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH)
def set_write_only(dir_path:str):
    os.chmod(dir_path, stat.S_IWRITE | stat.S_IWGRP | stat.S_IWOTH)
def set_read_write(dir_path:str):
    os.chmod(dir_path, stat.S_IWRITE | stat.S_IWGRP | stat.S_IWOTH | stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH)

def backup_files(source_dir:str, backup_dir:str):
    for filename in os.listdir(source_dir):
        shutil.copy2(os.path.join(source_dir, filename), backup_dir)
        with open(Logs,append_mode) as f:
            f.write(f"[Ops_backup:005/F/A] Backed up file\t{datetime.datetime.now()}\t{filename}\n")
def schedule_backup(source_dir:str, backup_dir:str, interval:int):
    while True:
        backup_files(source_dir, backup_dir)
        time.sleep(interval)
current_dir:str = os.getcwd()
current_dir = current_dir.replace("\\","/")
Storage_Compartment:str = "storage-br11"
backupStorage_Compartment:str = "backup-storage-stb-294"
Logs:str = "Logs"
append_mode:str = "a"
if not os.path.isdir(current_dir+"/"+Storage_Compartment):
    os.mkdir(current_dir+"/"+Storage_Compartment)
    with open(Logs,append_mode) as f:
        f.write(f"[Ops_makefile:001/F/A] Created Storage Compartment\t{datetime.datetime.now()}\t{Storage_Compartment}\n")
elif not os.path.isdir(current_dir+"/"+backupStorage_Compartment):
    os.mkdir(current_dir+"/"+backupStorage_Compartment)
    with open(Logs,append_mode) as f:
        f.write(f"[Ops_makefile:001/F/B] Created Storage Compartment\t{datetime.datetime.now()}\t{backupStorage_Compartment}\n")
else:
    pass 
if __name__ == '__main__':
    interval:int = 24 * 60 * 60 #seconds
    backup_dir:str = current_dir+"/"+backupStorage_Compartment
    source_dir:str = current_dir+"/"+Storage_Compartment
    schedule_backup(source_dir, backup_dir, interval)
    app.run(debug=False,port=8080,host="0.0.0.0")